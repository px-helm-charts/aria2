#!/usr/bin/env sh

sed -i "/host: l/c\        host: '${ARIA_HOST}'," /usr/share/nginx/html/app.js
sed -i "/auth: {}/c\        auth: { token: '${ARIA_TOKEN}' }," /usr/share/nginx/html/app.js

/docker-entrypoint.sh nginx -g "daemon off;"