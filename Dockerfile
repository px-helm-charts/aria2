FROM timonier/webui-aria2 AS stage-a

FROM nginx
COPY --from=stage-a /opt/webui-aria2/docs/ /usr/share/nginx/html/
COPY entrypoint.sh /
ENTRYPOINT /entrypoint.sh
